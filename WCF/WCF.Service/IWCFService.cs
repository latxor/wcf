﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WCF.Entities;

namespace WCF.Service
{
    [ServiceContract]
    public interface IWCFService
    {
        [OperationContract]
        List<Product> GetProduct();

        [OperationContract]
        List<Customer> GetCustomer();

        [OperationContract]
        void SubmitOrder(Order order);
    }
}

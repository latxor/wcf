﻿using System;
using System.Collections.Generic;
using System.Linq;

using System.Text;
using System.Threading.Tasks;
using WCF.Entities;
using WCF.Data;
using System.ServiceModel;
using WCF.Service;

namespace WCF.Service
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class WCFServices : IWCFService, IDisposable
    {
        readonly Context context = new Context();

     

        public List<Customer> GetCustomer()
        {
            return context.Customers.ToList();
        }

        public List<Product> GetProduct()
        {
            return context.Products.ToList();
        }

        [OperationBehavior(TransactionScopeRequired =true)]
        public void SubmitOrder(Order order)
        {
            context.Orders.Add(order);
            order.OrderItems.ForEach(oi => context.OrderItems.Add(oi));
            context.SaveChanges();
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}

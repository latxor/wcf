﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using WCF.Entities;

namespace WCF.Services
{
    [ServiceContract]
    public interface IWCFService
    {
        [OperationContract]
        List<Product> GetProduct();

        [OperationContract]
        List<Customer> GetCustomer();

        [OperationContract]
        void SubmitOrder(Order order);
    }
}
